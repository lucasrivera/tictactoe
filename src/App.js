import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class T3Piece extends Component{

  constructor(props) {
    super(props);
    this.state = {
      init:false,
      type:'_'
    }
  }

  render() {
    if(this.state.init || this.props.frozen){
      return <td>{this.state.type}</td>;
    }
    return <td onClick={this.initPiece}>_</td>;
  }

  initPiece = (e) => {
    this.setState({
      init:true,
      type:this.props.type
    });
    this.props.onClick(
      {
        xSlot:this.props.xSlot,
        ySlot:this.props.ySlot
      }
    );
  }
}

class T3Board extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isPlayer1Turn:true,
      whoWon:0
    }
  }

  currentBoard = [
    [0,0,0],
    [0,0,0],
    [0,0,0]
  ]

  playerToIcon = {
    true: 'O',
    false: 'X'
  };

  playerToNumber = {
    true: 1,
    false: 2
  }

  message(){
    if(this.state.whoWon === -1){
      //stalemate
      return <h3>Stalemate! Neither player wins. Reload the page to play again.</h3>;
    }
    else if(this.state.whoWon === 0){
      return <h3>Player {this.playerToNumber[this.state.isPlayer1Turn]}'s Turn: Place a {this.playerToIcon[this.state.isPlayer1Turn]} by clicking inside a box</h3>;
    }
    else{
      return <h3>Congradulations! Player {this.state.whoWon} Won. Reload the page to play again.</h3>;
    }
  }

  render() {
    return (
      <div>
        <table>
          <tbody>
            {
              [...Array(3)].map((x, i) =>
                <tr key={i}>
                  {
                    [...Array(3)].map((x, j) =>
                      <T3Piece key={i+"+"+j}
                      type={this.playerToIcon[this.state.isPlayer1Turn]}
                      onClick={this.updateBoard}
                      xSlot={i} ySlot={j}
                      frozen={this.state.whoWon}
                      />
                    )
                  }
                </tr>
              )
            }
          </tbody>
        </table>
        {this.message()}
      </div>
    );
  }

  wins = [
    [[0, 0], [0, 1], [0, 2]],//hTop
    [[1, 0], [1, 1], [1, 2]],//hMid
    [[2, 0], [2, 1], [2, 2]],//hEnd
    [[0, 0], [1, 0], [2, 0]],//vTop
    [[0, 1], [1, 1], [2, 1]],//vMid
    [[0, 2], [1, 2], [2, 2]],//vEnd
    [[0, 0], [1, 1], [2, 2]],//dBack
    [[0, 2], [1, 1], [2, 0]]//dFow
  ]

  updateBoard = (e) => {
    var localWhoWon = 0
    const currentPlayer = this.playerToNumber[this.state.isPlayer1Turn];
    this.currentBoard[e.xSlot][e.ySlot] = currentPlayer;
    //Now that the board's data has been updated,
    //we can check to see if player 1 or 2 got the win or stalemate
    var empty = false;
    for(let i=0;i<this.currentBoard.length;i++){
      for(let j=0;j<this.currentBoard.length;j++){
        //found an unseen piece
        if(this.currentBoard[i][j] === 0){
          empty = true;
        }
      }
    }

    for(let pos=0;pos<this.wins.length;pos++){
      let line = this.wins[pos];
      let con1 = this.currentBoard[line[0][0]][line[0][1]];
      let con2 = this.currentBoard[line[1][0]][line[1][1]];
      let con3 = this.currentBoard[line[2][0]][line[2][1]];
      if((currentPlayer === con1) &&
      (currentPlayer === con2) &&
      (currentPlayer === con3) ){
        localWhoWon = currentPlayer;
      }
    }

    if(!empty && localWhoWon === 0){
      localWhoWon = -1;
    }
    this.setState((prevState, props) => ({
      isPlayer1Turn: !prevState.isPlayer1Turn,
      whoWon:localWhoWon
    }));
  }
}

class App extends Component {
  render() {
    return (
      <T3Board/>
    );
  }
}

export default App;
